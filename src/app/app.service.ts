import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  private subjectSource = new Subject<string>();
  subjectMessage = this.subjectSource.asObservable();

  private behaviorSource = new BehaviorSubject<string>('Some initial message');
  behaviorMessage = this.behaviorSource.asObservable();

  text = 'some text';

  constructor() { }

  changeSubjectMessage(message: string) {
    this.subjectSource.next(message);
  }

  changeBehaviourMessage(message: string) {
    this.behaviorSource.next(message);
  }

  changeText(message: string) {
    this.text = message;
  }
}
