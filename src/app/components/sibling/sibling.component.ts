import { Component, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-sibling',
  templateUrl: './sibling.component.html',
  styleUrls: ['./sibling.component.scss']
})
export class SiblingComponent implements OnInit, OnChanges, OnDestroy {

  inputText = '';

  messageShared: string;

  constructor(private appService: AppService) { }

  ngOnInit(): void {
    // this.appService.currentMessage.subscribe(message => this.messageShared = message);
  }

  ngOnChanges(): void {
  }

  ngOnDestroy(): void {
  }

}
