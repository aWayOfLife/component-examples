import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss']
})
export class ChildComponent implements OnInit, OnChanges {

  @Input() textFromParent;
  @Input() trigger;

  @Output() messageEvent = new EventEmitter<string>();

  isButtonDisabled = false;
  message = 'Text from child';
  // anotherMessage = 'Another text from child';

  constructor(private appService: AppService) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    // if (this.trigger) {
    //   this.sendMessage();
    // }
  }

  sendMessage() {
    this.messageEvent.emit(this.message);
    // this.isButtonDisabled = true;
    // this.appService.changeMessage(this.message);
  }

}
