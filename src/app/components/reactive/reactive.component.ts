import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl, AsyncValidatorFn, ValidationErrors } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { map, delay } from 'rxjs/operators';


export interface User {
  email: string;
  password: string;
  contact: number;
}

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styleUrls: ['./reactive.component.scss']
})

export class ReactiveComponent implements OnInit {
  userForm: FormGroup;

  private validContacts = ['1111', '2222', '3333', '4444'];


  constructor() { }

  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
    this.userForm = new FormGroup ({
      email: new FormControl('', [Validators.required, Validators.minLength(6)] , []),
      password: new FormControl('', Validators.compose([Validators.required, this.passwordValidator()])),
      contact: new FormControl('', [], [this.contactValidator()])
    });
  }

  passwordValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (!control.value) {
        return null;
      }
      const regex = new RegExp('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$');
      const valid = regex.test(control.value);
      return valid ? null : { invalidPassword: true };
    };
  }

   contactValidator(): AsyncValidatorFn {
     return (control: AbstractControl) => {
      if (!control.value) {
        return of(null);
      }
      return this.fakeHttp(control.value).pipe(
        map((result: boolean) => result ? null : {invalidContact: true})
      );
   };
  }


  fakeHttp(value: string) {
    return of(this.validContacts.includes(value.toString())).pipe(delay(500));
  }

  onSubmit() {
    console.log(this.userForm.value);
  }

}
