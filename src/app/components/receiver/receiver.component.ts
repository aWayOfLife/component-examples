import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-receiver',
  templateUrl: './receiver.component.html',
  styleUrls: ['./receiver.component.scss']
})
export class ReceiverComponent implements OnInit, OnChanges {
  textString = this.appService.text;
  textFromSubject: string;
  textFromBehaviorSubject: string;

  constructor(private appService: AppService) { }


  ngOnChanges(changes: SimpleChanges): void {
    // this.textString = this.appService.text;
  }

  ngOnInit(): void {
    this.appService.subjectMessage.subscribe(message => this.textFromSubject = message);
    this.appService.behaviorMessage.subscribe(message => this.textFromBehaviorSubject = message);
  }

}
