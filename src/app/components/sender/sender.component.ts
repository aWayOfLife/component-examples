import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-sender',
  templateUrl: './sender.component.html',
  styleUrls: ['./sender.component.scss']
})
export class SenderComponent implements OnInit {
  message = '';

  constructor(private appService: AppService) { }

  ngOnInit(): void {
  }

  sendMessage() {
    this.appService.changeSubjectMessage(this.message);
    this.appService.changeBehaviourMessage(this.message);
    this.appService.changeText(this.message);
  }
}
