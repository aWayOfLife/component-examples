import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { ChildComponent } from '../child/child.component';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.scss']
})
export class ParentComponent implements OnInit, AfterViewInit {

  textFromParent = 'Text from parent';
  textFromChild = 'old message';
  trigger = false;
  // anotherTextFromChild = '';
  // @ViewChild(ChildComponent) child;

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    // this.anotherTextFromChild = this.child.anotherMessage;
  }

  receiveMessage($event) {
    this.textFromChild = $event;
  }

  sendMessageFromParent() {
    this.trigger = true;
  }

}
